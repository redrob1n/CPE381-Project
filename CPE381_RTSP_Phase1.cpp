/*
 * File name: CPE381_RTSP_Phase1.cpp
 * Brief: Parses .wav file sample by sample and adds a 1.6 kHz sine wave to
 *      the input signal to simulate noise
 * Author: Chandler Ellis
 * Date: 03/07/2018
 * Input: ellis_c_orig.wav
 * Output: ellis_c_mod.wav
 *
 */

#include <iostream>
#include <cstdint> // for uintX_t types
#include <ctime>
#include <cmath>
#include <fstream>
using namespace std;

enum channel_state {
    Mono = 1,
    Stereo = 2
};

struct wav_header_t {
  uint8_t chunkID[4]; // contains "RIFF" in ascii
  uint32_t chunkSize; // size of the chunk following this number
  uint8_t format[4];  // contains the letters "WAVE"
  uint8_t subChunkID[4]; // contains the letters "fmt "
  uint32_t subChunkSize;
  uint16_t audioFormat; // anything other than 1 signifies compression
  uint16_t numChannels; // mono = 1, stereo = 2
  uint32_t sampleRate; // the sample rate of the audio signal
  uint32_t byteRate; // sampleRate * numChannels * Bits per sample/ 8
  uint16_t blockAlign; // number of bytes for one sample including all numChannels
  uint16_t bitsPerSample; // 8 or 16 bits
};

struct chunk_t {
    uint8_t dataChunkID[4]; // "data" in big edian
    uint32_t dataChunkSize; // numspamples * num channels * bitspersample/8
};

struct output_information_t {
    uint16_t sample_freq;
    uint16_t num_channels;
    uint16_t record_length;
};

void generateNoisySignal(const char*, const char*, output_information_t&);

int main(int argc, char const **argv)
{
    clock_t t = clock(); // time consumed by the program

    cout << "Welcome!" << endl;

    output_information_t info;
    generateNoisySignal("ellis_c_orig.wav", "ellis_c_mod.wav", info);

    t = clock() - t; // compute number of clock ticks since inital clock

    float tSeconds = t/CLOCKS_PER_SEC; // compute number of seconds since inital clock

    ofstream outFile;
    outFile.open("summary.txt"); // generate output text file
    if(outFile.is_open())
    {
        outFile << "Input filename: ellis_c_orig.wav" << endl;
        outFile << "Ouput filename: ellis_c_mod.wav" << endl;
        outFile << "Sampling Frequency: " << info.sample_freq << endl;
        outFile << "Number of Channels: " << info.num_channels << endl;
        outFile << "Record Length (s): " << info.record_length << endl;
        outFile << "Clock ticks consumed: " << t << endl;
        outFile << "Time consumed by program (s): " << tSeconds <<endl;
        outFile.close();
    }

    cout << "Program complete, summary.txt and ellis_c_mod.wav have been generated." << endl;
    return 0;
}


void generateNoisySignal(const char *input_filename, const char *output_filename, output_information_t& info)
{
    FILE *ifp; // output file pointer
    FILE *ofp; // input file pointer

    ifp = fopen(input_filename, "rb"); // open input file as in binary form

    if(!ifp) // handle file error
    {
        cout << "Error input file: " << input_filename <<"file could not be opened" << endl;
        cout << "Exiting..." << endl;
        return;
    }
    else
    {
        cout << "Input file: " << input_filename << " opened successfully" << endl;
    }

    ofp = fopen(output_filename, "w");

    if(!ofp) // handle file error
    {
        cout << "Error opening output file: " << output_filename << endl;
        cout << "Exiting..." << endl;
        return;
    }
    else
    {
        cout << "Output file: " << output_filename << " open successfully" << endl;
    }

    struct wav_header_t header;

    fread(&header, sizeof(header), 1, ifp);

    chunk_t data_chunk; // data chunk information struct

    fwrite(&header, sizeof(header), 1, ofp); // write header info

    // find data chunk 0x61746164 = "data" big edian
    do
    {
        fread(&data_chunk, sizeof(data_chunk), 1, ifp);
    }while(*(uint32_t *)&data_chunk.dataChunkID != 0x61746164);


    fwrite(&data_chunk, sizeof(data_chunk), 1, ofp); // write data chunk

    int num_samples = data_chunk.dataChunkSize * 8 / (header.bitsPerSample * header.numChannels);

    int sample_size = header.bitsPerSample / 8; // size in bytes of a sample i.e. mono -1 stereo -2

    float Ts = 1 / (float)header.sampleRate; // get period of the input signal

    fpos_t curr_pos;
    fgetpos(ifp, &curr_pos);
    int16_t data, amp = 0;

    for(int i = 0; i < num_samples *2; i++) // get the maximum amplitude of the input signal
    {
        fread(&data, sizeof(int16_t), 1, ifp);
        if(abs(data) > amp)
            amp = data;
    }

    float amplitude = amp *0.25; // amplitude is 1/4 of the maxium amplitude of the input signal
    fclose(ifp);
    ifp = fopen("ellis_c_orig.wav", "rb");
    fsetpos(ifp, &curr_pos);

    if(sample_size == Mono)
    {
        int8_t *sound_data = new int8_t[num_samples]; // dynamically allocate array

        int8_t previous_sample = 0;

        for(int i = 0; i < num_samples; i++)
        {
            fread(&sound_data[i], sample_size, 1, ifp);
            sound_data[i] = (sound_data[i] +previous_sample) / 2 + amplitude*sin(2*M_PI*1600*Ts*i);
            fwrite(&sound_data[i], sizeof(int8_t), 1, ofp);
            previous_sample = sound_data[i];
        }

        delete[] sound_data; // deallocate sound data array
    }
    else if(sample_size == Stereo)
    {
        int16_t *sound_data = new int16_t[num_samples*2]; // dynamically allocate sound data array

        int16_t previous_sample = 0;
        int16_t current_sample = 0;
        for(int i = 0; i < num_samples*2; i++)
        {
            fread(&sound_data[i], sample_size, 1, ifp);
            current_sample = sound_data[i];

            //average current and previous sample and add sine wave
            sound_data[i] = (sound_data[i] +previous_sample) / 2 + amplitude*sin(2*M_PI*1600*Ts*i);
            fwrite(&sound_data[i], sizeof(int16_t), 1, ofp);
            previous_sample = current_sample;
        }

        delete[] sound_data; // deallocate sound data array
    }

    info.sample_freq = header.sampleRate;
    info.num_channels = header.numChannels;
    info.record_length = Ts*num_samples; //calculate length of audio recording

    fclose(ofp);
    fclose(ifp);
}

